﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media.Imaging;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Point = System.Drawing.Point;
using Size = System.Drawing.Size;

namespace OSIRV_Projekt.Model
{
    public class ImageModel : IDisposable
    {
        private static Mat EmptyImage => new Mat(100,100, DepthType.Cv8U, 1);

        public string FileName { get; private set; } = "";

        public BitmapSource Image => _imageMat.ToBitmapSource();
        public BitmapSource HsvImage => _hsvMat.ToBitmapSource();

        private Mat _imageMat;
        private Mat _hsvMat = new Mat();
        private Mat _maskMat = new Mat();
        private Mat _thresholdMat = new Mat();

        public ImageModel(string imagePath)
        {
            try
            {
                _imageMat = CvInvoke.Imread(imagePath);

                var fileNameLong = imagePath.Split('\\').Last();
                var fileNameParts = fileNameLong.Split('.');
                FileName = fileNameParts[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return;
            }

            CvInvoke.CvtColor(_imageMat, _hsvMat, ColorConversion.Bgr2Hsv);
        }

        ~ImageModel()
        {
            Dispose(false);
        }

        private void ReleaseUnmanagedResources()
        {
            // TODO release unmanaged resources here
            FileName = null;
        }

        private void Dispose(bool disposing)
        {
            ReleaseUnmanagedResources();
            if (disposing)
            {
                _imageMat?.Dispose();
                _hsvMat?.Dispose();
                _maskMat?.Dispose();
                _thresholdMat?.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Generates bit mask, based on min and max HSV values. Values withing range are set to 255 (1), else to 0.
        /// </summary>
        /// <param name="minHsv"></param>
        /// <param name="maxHsv"></param>
        /// <returns>Bit mask.</returns>
        public Mat GenerateMask(Hsv minHsv, Hsv maxHsv, int ksize)
        {
            if (_hsvMat == null) return EmptyImage;

            var lower = new ScalarArray(minHsv.MCvScalar);
            var upper = new ScalarArray(maxHsv.MCvScalar);

            try
            {
                CvInvoke.InRange(_hsvMat, lower, upper, _maskMat);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return EmptyImage;
            }

            return _maskMat;
        }

        /// <summary>
        /// Sets pixel values to (maxHue, 255, 200) HSV value, based on given image mask.
        /// </summary>
        /// <param name="maxHue"></param>
        /// <returns>HSV image with more intense colors.</returns>
        public Mat SetValueToHsvImageWithMask(byte maxHue)
        {
            if (_hsvMat == null || _maskMat == null) return EmptyImage;

            Mat updatedHsv = new Mat();
            _hsvMat.CopyTo(updatedHsv);

            try
            {
                updatedHsv.SetTo(new MCvScalar(maxHue, 255, 200), _maskMat);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return EmptyImage;
            }

            return updatedHsv;
        }

        /// <summary>
        /// Applies thresholding based on passed parameters to the passed hsv image.
        /// Note! - You should call SetValueToHsvImageWithMask and used the returned Mat as the hsvMat parameter.
        /// </summary>
        /// <param name="hsvMat"></param>
        /// <param name="threshold"></param>
        /// <param name="ksize"></param>
        /// <param name="sigX"></param>
        /// <param name="sigY"></param>
        /// <returns>Thresheld image.</returns>
        public Mat ApplyThresholdingWithMedianBlur(Mat hsvMat, double threshold = 90d, int ksize = 9)
        {
            try
            {
                Mat rgbMat = new Mat();
                CvInvoke.CvtColor(hsvMat, rgbMat, ColorConversion.Hsv2Rgb);
                Mat gray = new Mat();
                CvInvoke.CvtColor(rgbMat, gray, ColorConversion.Rgb2Gray);

                CvInvoke.MedianBlur(gray, gray, ksize);

                CvInvoke.Threshold(gray, _thresholdMat, threshold, 255d, ThresholdType.BinaryInv);

                return _thresholdMat;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return EmptyImage;
            }
        }

        /// <summary>
        /// Applies contouring on image based on passed threshol image and returns an array(Mat) of contours.
        /// </summary>
        /// <param name="minAreaSize">The minimal area a contour has to have to be considered a cell.</param>
        /// <param name="cellCount">The number of cells that were detected in the image.</param>
        /// <param name="cellCoordinates">The locations of the cells on the image.</param>
        /// <returns>Contoured image.</returns>
        public Mat ApplyContouring(int minAreaSize, out int cellCount, out List<MCvPoint2D64f> cellCoordinates)
        {
            if (_thresholdMat == null)
            {
                cellCount = 0;
                cellCoordinates = null;
                return EmptyImage;
            }
            
            try
            {
                using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
                using (VectorOfVectorOfPoint filteredContours = new VectorOfVectorOfPoint())
                {
                    CvInvoke.FindContours(_thresholdMat, contours, null, RetrType.List, ChainApproxMethod.ChainApproxSimple);

                    var contourCount = contours.Size;

                    var coordinates = new List<MCvPoint2D64f>();

                    for (var i = 0; i < contourCount; i++)
                    {
                        using (VectorOfPoint contour = contours[i])
                        using (VectorOfPoint approxContour = new VectorOfPoint())
                        {
                            CvInvoke.ApproxPolyDP(contour, approxContour, CvInvoke.ArcLength(contour, true) * 0.05, true);

                            if (CvInvoke.ContourArea(approxContour) > minAreaSize)
                            {
                                filteredContours.Push(contour);

                                using (var moments = CvInvoke.Moments(contour))
                                    coordinates.Add(moments.GravityCenter);
                            }
                        }
                    }

                    Mat contouredImage = new Mat();
                    _imageMat.CopyTo(contouredImage);
                    CvInvoke.DrawContours(contouredImage, filteredContours, -1, new MCvScalar(0, 0, 255), 3);

                    cellCount = filteredContours.Size;
                    cellCoordinates = coordinates;
                    return contouredImage;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                cellCount = 0;
                cellCoordinates = null;
                return EmptyImage;
            }
        }
    }
}
