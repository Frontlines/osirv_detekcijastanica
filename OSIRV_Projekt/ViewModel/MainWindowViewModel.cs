﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using System.Xml.Serialization;
using Emgu.CV;
using Emgu.CV.Structure;
using GalaSoft.MvvmLight.Command;
using OSIRV_Projekt.Model;
using MessageBox = System.Windows.MessageBox;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;

namespace OSIRV_Projekt.ViewModel
{
    public class MainWindowViewModel : ViewModelBase
    {
        #region SaveLoadSettings
        public struct SaveData
        {
            public byte MaxHue;
            public byte MinHue;
            public byte MaxSat;
            public byte MinSat;
            public byte MaxVal;
            public byte MinVal;
            public byte Threshold;
            public byte KernelSize;
            public int MinAreaSize;
            public string SavePath;
        }

        private void Save()
        {
            XmlSerializer writer = new XmlSerializer(typeof(SaveData));

            SaveData data = new SaveData
            {
                MaxHue = HueMax,
                MinHue = HueMin,
                MaxSat = SatMax,
                MinSat = SatMin,
                MaxVal = ValMax,
                MinVal = ValMin,
                Threshold = Threshold,
                KernelSize = KernelSize,
                MinAreaSize = MinAreaSize,
                SavePath = SavePath
            };

            try
            {
                var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                           "//CellRecognitionSoftwareSettings.xml";
                FileStream stream = File.Create(path);
                writer.Serialize(stream, data);
                stream.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void Load()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                       "//CellRecognitionSoftwareSettings.xml";

            if (File.Exists(path))
            {
                try
                {
                    XmlSerializer reader = new XmlSerializer(typeof(SaveData));

                    StreamReader stream = new StreamReader(path);

                    SaveData data = (SaveData)reader.Deserialize(stream);

                    stream.Close();

                    HueMax = data.MaxHue;
                    HueMin = data.MinHue;
                    SatMax = data.MaxSat;
                    SatMin = data.MinSat;
                    ValMax = data.MaxVal;
                    ValMin = data.MinVal;
                    Threshold = data.Threshold;
                    KernelSize = data.KernelSize;
                    MinAreaSize = data.MinAreaSize;
                    SavePath = data.SavePath;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
        }
        #endregion

        public MainWindowViewModel()
        {
            Load();

            _openFileDialog.Title = "Open Image";
            _openFileDialog.CheckFileExists = true;
            _openFileDialog.CheckPathExists = true;
            _openFileDialog.Multiselect = true;

            _savePathDialog.Description = "Processed Image Save Location";
            _savePathDialog.ShowNewFolderButton = true;
            _savePathDialog.SelectedPath = SavePath;

            Application.ApplicationExit += (sender, args) => Save();
        }

        private OpenFileDialog _openFileDialog = new OpenFileDialog();
        private FolderBrowserDialog _savePathDialog = new FolderBrowserDialog();
        public string SavePath { get; set; } = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        private string[] _fileNames;
        private ImageModel _currentImage;
        public int TotalCellCount { get; set; }

        public string Logs { get; set; }

        #region MaskValues
        //hmin = 120, hmax = 140, smin = 160, smax = 255, valmin = 0, valmax = 255
        private byte _hueMin = 0;
        public byte HueMin
        {
            get => _hueMin;
            set
            {
                if (value > 180)
                {
                    _hueMin = 180;
                }

                _hueMin = value;

                UpdateMask();
            }
        }
        private byte _hueMax = 180;
        public byte HueMax
        {
            get => _hueMax;
            set
            {
                if (value > 180)
                {
                    _hueMax = 180;
                }

                _hueMax = value;

                UpdateMask();
            }
        }

        private byte _satMin = 0;
        public byte SatMin
        {
            get => _satMin;
            set
            {
                _satMin = value;
                UpdateMask();
            }
        }
        private byte _satMax = 255;
        public byte SatMax
        {
            get => _satMax;
            set
            {
                _satMax = value;
                UpdateMask();
            }
        }

        private byte _valMin = 0;
        public byte ValMin
        {
            get => _valMin;
            set
            {
                _valMin = value;
                UpdateMask();
            }
        }
        private byte _valMax = 255;
        public byte ValMax
        {
            get => _valMax;
            set
            {
                _valMax = value;
                UpdateMask();
            }
        }
        #endregion

        #region ParameterValues
        private byte _threshold = 90;
        public byte Threshold
        {
            get => _threshold;
            set
            {
                _threshold = value;
                UpdateThreshold();
            }
        }

        private byte _kernelSize = 9;
        public byte KernelSize
        {
            get => _kernelSize;
            set
            {
                if (value % 2 == 0)
                {
                    if (value <= 0)
                    {
                        _kernelSize = 1;
                    }
                    else if (value < _kernelSize)
                    {
                        _kernelSize = value;
                        _kernelSize--;
                    }
                    else
                    {
                        _kernelSize = value;
                        _kernelSize++;
                    }
                }
                else
                {
                    _kernelSize = value;
                }
                
                UpdateThreshold();
            }
        }

        private int _minAreaSize;

        public int MinAreaSize
        {
            get => _minAreaSize;
            set
            {
                _minAreaSize = value < 0 ? 0 : value;

                UpdateContours();
            }
        }
        #endregion

        public BitmapSource ActiveImage { get; set; }

        public RelayCommand OpenImageCommand => new RelayCommand(OpenImageButton);
        private void OpenImageButton()
        {
            var result = _openFileDialog.ShowDialog();

            if (result.HasValue && result.Value)
            {
                _fileNames = _openFileDialog.FileNames;

                try
                {
                    _currentImage = new ImageModel(_fileNames[0]);

                    Logs += "Image/s Open - Success\n";

                    ActiveImage = _currentImage.Image;

                    ProcessImage();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                    return;
                }
            }
        }

        public RelayCommand ProcessImagesCommand => new RelayCommand(ProcessAllImages);
        public RelayCommand ShowContoursCommand => new RelayCommand(UpdateContours);
        public RelayCommand ClearLogsCommand => new RelayCommand(()=> { Logs = ""; });
        public RelayCommand PickSavePathCommand => new RelayCommand(() =>
        {
            var result = _savePathDialog.ShowDialog();

            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(_savePathDialog.SelectedPath))
            {
                SavePath = _savePathDialog.SelectedPath;
            }
        });

        /// <summary>
        /// Runs through all the image processes based on passed settings and finds cell contours for the current image.
        /// </summary>
        private void ProcessImage()
        {
            UpdateMask();
            UpdateThreshold();
            UpdateContours();
        }

        /// <summary>
        /// Runs through all the images and finds the cells based on the set parameters.
        /// </summary>
        private void ProcessAllImages()
        {
            if (_fileNames.Length <= 0) return;

            Logs = "";
            Logs += "Beginning Image Processing for opened images.\n";
            TotalCellCount = 0;

            int totalCellCount = 0;

            Parallel.ForEach(_fileNames, new ParallelOptions {MaxDegreeOfParallelism = 4}, file =>
            {
                using (var image = OpenImage(file))
                {

                    UpdateMask(image);
                    UpdateThreshold(image);

                    int cellCount = 0;
                    Mat resultMat = new Mat();
                    UpdateContours(image, ref cellCount, ref resultMat);

                    Interlocked.Add(ref totalCellCount, cellCount);

                    resultMat.Save(SavePath + "//" + image.FileName + "_cells.tiff");
                }
            });

            TotalCellCount = totalCellCount;
        }

        /// <summary>
        /// Tries to open an image at the given path and sets the _currentImage to the new ImageModel.
        /// </summary>
        /// <param name="file"></param>
        private ImageModel OpenImage(string file)
        {
            try
            {
                var image = new ImageModel(file);

                return image;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);

                return null;
            }
        }

        private void UpdateMask()
        {
            if(_currentImage == null) return;

            var lowRange = new Hsv(HueMin, SatMin, ValMin);
            var highRange = new Hsv(HueMax, SatMax, ValMax);

            var mask = _currentImage.GenerateMask(lowRange, highRange, KernelSize);

            ActiveImage = mask.ToBitmapSource();
        }

        private void UpdateMask(ImageModel image)
        {
            if (image == null) return;

            var lowRange = new Hsv(HueMin, SatMin, ValMin);
            var highRange = new Hsv(HueMax, SatMax, ValMax);

            var mask = image.GenerateMask(lowRange, highRange, KernelSize);
        }

        private void UpdateThreshold()
        {
            if (_currentImage == null) return;

            var hsvMat = _currentImage.SetValueToHsvImageWithMask(HueMax);
            var thresholdMat = _currentImage.ApplyThresholdingWithMedianBlur(hsvMat, Threshold, KernelSize);

            ActiveImage = thresholdMat.ToBitmapSource();
        }

        private void UpdateThreshold(ImageModel image)
        {
            if (image == null) return;

            var hsvMat = image.SetValueToHsvImageWithMask(HueMax);
            var thresholdMat = image.ApplyThresholdingWithMedianBlur(hsvMat, Threshold, KernelSize);
        }

        private void UpdateContours(ImageModel image, ref int cellCount, ref Mat contouredMat)
        {
            if (_currentImage == null) return;

            contouredMat = image.ApplyContouring(MinAreaSize, out cellCount, out var coords);

            lock (Logs)
            {
                Logs += "Image " + image.FileName + " processed - found " + cellCount + " cells.\n";
                Logs += "Cell locations: ";

                if (coords != null)
                    foreach (var coord in coords)
                    {
                        Logs += "(" + coord.X.ToString("####") + ", " + coord.Y.ToString("####") + "), ";
                    }

                Logs += "\n";
            }
        }

        private void UpdateContours()
        {
            if (_currentImage == null) return;

            var contourMat = _currentImage.ApplyContouring(MinAreaSize, out var cellCount, out var coords);

            ActiveImage = contourMat.ToBitmapSource();

            Logs += "Image " + _currentImage.FileName + " processed - found " + cellCount + " cells.\n";
            Logs += "Cell locations: ";

            foreach (var coord in coords)
            {
                Logs += "(" + coord.X.ToString("####") + ", " + coord.Y.ToString("####") + "), ";
            }

            Logs += "\n";
        }
    }
}
