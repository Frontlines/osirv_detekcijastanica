﻿using System.ComponentModel;
using PropertyChanged;

namespace OSIRV_Projekt.ViewModel
{
    [AddINotifyPropertyChangedInterface]
    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };
    }
}
